import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Button, Form, Icon, Image, Message, Segment } from 'semantic-ui-react';

import styles from './styles.module.scss';

const AddPost = ({
  addPost,
  uploadImage
}) => {
  const [body, setBody] = useState('');
  const [image, setImage] = useState(undefined);
  const [isUploading, setIsUploading] = useState(false);
  const [isError, setError] = useState(false);

  const handleAddPost = async () => {
    setError(false);
    if (!body) {
      return;
    }
    await addPost({ imageId: image?.imageId, body });
    setBody('');
    setImage(undefined);
  };

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    setError(false);
    try {
      const { id: imageId, link: imageLink } = await uploadImage(target.files[0]);
      setImage({ imageId, imageLink });
    } catch {
      setError(true);
    } finally {
      setIsUploading(false);
    }
  };

  return (
    <Segment>
      <Form onSubmit={handleAddPost} error={isError}>
        <Form.TextArea
          name="body"
          value={body}
          placeholder="What is the news?"
          onChange={ev => setBody(ev.target.value)}
        />
        {image?.imageLink && (
          <div className={styles.imageWrapper}>
            <Image className={styles.image} src={image?.imageLink} alt="post" />
          </div>
        )}
        <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
          <Icon name="image" />
          Attach image
          <input name="image" type="file" onChange={handleUploadFile} hidden />
        </Button>
        <Button floated="right" color="blue" type="submit">Post</Button>
        <Message error>
          Error! Can`t load image!
        </Message>
      </Form>
    </Segment>
  );
};

AddPost.propTypes = {
  addPost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired
};

export default AddPost;
