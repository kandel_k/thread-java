import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Stomp } from '@stomp/stompjs';
import SockJS from 'sockjs-client';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';

const Notifications = ({ user, applyPost, deletePost }) => {
  const [stompClient] = useState(Stomp.over(new SockJS('/ws')));

  useEffect(() => {
    if (!user) {
      return undefined;
    }

    stompClient.debug = () => { };
    stompClient.connect({}, () => {
      console.log('connected');

      const { id } = user;

      stompClient.subscribe('/topic/post/like/', message => {
        const like = JSON.parse(message.body);

        if (!!like?.id && like.postOwnerId === id && like.isLike === true && like.userId !== id) {
          NotificationManager.info('Your post was liked');
        }
        // reactionPost(like);
      });

      stompClient.subscribe('/topic/comment/like/', message => {
        const like = JSON.parse(message.body);
        if (like.id && like.commentOwnerId === id && like.isLike === true && like.userId !== id) {
          NotificationManager.info('Your comment was liked');
        }
        // reactionComment(like);
      });

      stompClient.subscribe('/topic/comment/new/', message => {
        const comment = JSON.parse(message.body);
        if (comment.postOwnerId === id && comment.userId !== id) {
          NotificationManager.info('New comment');
        }
      });

      stompClient.subscribe('/topic/post/delete/', message => {
        const post = JSON.parse(message.body);
        if (post.id) {
          if (post.userId !== id) {
            deletePost(post.id);
          } else {
            NotificationManager.info('Your post has been deleted');
          }
        }
      });

      stompClient.subscribe('/topic/post/new/', message => {
        const post = JSON.parse(message.body);
        if (post.userId !== id) {
          applyPost(post.id);
        }
      });
    });

    return () => {
      stompClient.disconnect();
    };
  });

  return <NotificationContainer />;
};

Notifications.defaultProps = {
  user: undefined
};

Notifications.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  applyPost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired
  // reactionPost: PropTypes.func.isRequired
};

export default Notifications;
