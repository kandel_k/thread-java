import React from 'react';
import PropTypes from 'prop-types';
import { Button, Icon, Label, Comment as CommentUI } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

const Comment = ({
  comment: { body, createdAt, user, id, likeCount, dislikeCount },
  userId,
  postId,
  deleteComment: delComment,
  reactionComment: reactComment
}) => (
  <CommentUI className={styles.comment}>
    <CommentUI.Avatar src={getUserImgLink(user.image)} />
    <CommentUI.Content>
      <CommentUI.Author as="a">
        {user.username}
      </CommentUI.Author>
      <CommentUI.Metadata>
        {moment(createdAt).fromNow()}
      </CommentUI.Metadata>
      <CommentUI.Text>
        {body}
      </CommentUI.Text>
      <CommentUI.Actions>
        <CommentUI.Action onClick={() => reactComment(id, true)}>
          <Button as="div" labelPosition="right">
            <Button color="red" size="mini">
              <Icon name="heart" />
              Like
            </Button>
            <Label basic color="red" pointing="left">
              {likeCount}
            </Label>
          </Button>
        </CommentUI.Action>
        <CommentUI.Action onClick={() => reactComment(id, false)}>
          <Button as="div" labelPosition="right">
            <Button icon color="blue" size="mini">
              <Icon name="thumbs down outline" />
              Dislike
            </Button>
            <Label basic pointing="left">
              {dislikeCount}
            </Label>
          </Button>
        </CommentUI.Action>
        {userId === user.id && <CommentUI.Action onClick={() => delComment(id, postId)}>Delete</CommentUI.Action>}
      </CommentUI.Actions>
    </CommentUI.Content>
  </CommentUI>
);

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  userId: PropTypes.string.isRequired,
  deleteComment: PropTypes.func.isRequired,
  postId: PropTypes.string.isRequired,
  reactionComment: PropTypes.func.isRequired
};

export default Comment;
