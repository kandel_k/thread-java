import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Card, Icon, Image, Label, Loader, Popup } from 'semantic-ui-react';
import { connect } from 'react-redux';
import moment from 'moment';

import styles from './styles.module.scss';

const Post = ({
  post,
  reactionPost,
  toggleExpandedPost,
  sharePost, userId,
  deletePost,
  loadPostLikes
}) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;
  const date = moment(createdAt).fromNow();

  const [likes, setLikes] = useState(null);
  const [isLoading, setLoading] = useState(false);
  // const [isDisabled, setDisabled] = useState(true);

  const loadLikes = async () => {
    setLoading(true);
    setLikes(await loadPostLikes(id));
    setLoading(false);
  };

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description>
          {body}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Popup
          trigger={(
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => reactionPost(id, true)}>
              <Icon name="thumbs up" />
              {likeCount}
            </Label>
          )}
          on="hover"
          mouseEnterDelay={1000}
          position="bottom center"
          onOpen={loadLikes}
          disabled={likeCount === 0}
        >
          <Loader size="mini" active={isLoading} />
          {likes && likes.map(like => <p key={like.userId}>{like.username}</p>)}
        </Popup>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => reactionPost(id, false)}>
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
        {user.id === userId
          && (
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => deletePost(id)}>
              <Icon name="trash alternate" />
            </Label>
          )}
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  reactionPost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired,
  deletePost: PropTypes.func.isRequired,
  loadPostLikes: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  userId: rootState.profile.user.id
});

export default connect(
  mapStateToProps,
  null
)(Post);
