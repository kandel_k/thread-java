import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Redirect, useLocation } from 'react-router-dom';
import { Form, Button, Segment, Message } from 'semantic-ui-react';

const ResetPasswordEmailForm = ({ resetPassword: reset }) => {
  const [isLoading, setLoading] = useState(false);
  const [isPasswordEqual, setPasswordEquality] = useState(false);
  const [isError, setError] = useState(false);
  const [isSuccess, setSuccess] = useState(false);
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  const [token] = useState(new URLSearchParams(useLocation().search).get('token'));

  const password1Changed = password => {
    setPassword1(password);
  };

  const password2Changed = password => {
    setPassword2(password);
  };

  const resetPassword = async () => {
    if (!isPasswordEqual || isLoading) {
      return;
    }
    setError(false);
    setLoading(true);
    try {
      await reset({
        password: password1,
        token
      });
    } catch (error) {
      setError(true);
      setLoading(false);
      return;
    }
    setSuccess(true);
  };

  return (
    <Form name="sendResetEmailForm" size="large" onSubmit={resetPassword} error={isError} warning={!isPasswordEqual}>
      <Segment>
        <Form.Input
          fluid
          icon="at"
          iconPosition="left"
          placeholder="Password"
          type="password"
          onBlur={() => setPasswordEquality(password1 === password2)}
          onChange={ev => password1Changed(ev.target.value)}
        />
        <Form.Input
          fluid
          icon="at"
          iconPosition="left"
          placeholder="Confirm password"
          type="password"
          onBlur={() => setPasswordEquality(password1 === password2)}
          onChange={ev => password2Changed(ev.target.value)}
        />
        <Button type="submit" color="teal" fluid size="large" loading={isLoading} disabled={!isPasswordEqual} primary>
          Reset password
        </Button>
      </Segment>
      <Message error>
        <Message.Header>Error</Message.Header>
        Something went wrong! Please try again
      </Message>
      <Message warning>
        <Message.Header>Warning</Message.Header>
        <p>
          Passwords are not equal
        </p>
      </Message>
      {isSuccess && <Redirect to="/login" />}
    </Form>
  );
};

ResetPasswordEmailForm.propTypes = {
  resetPassword: PropTypes.func.isRequired
};

export default ResetPasswordEmailForm;
