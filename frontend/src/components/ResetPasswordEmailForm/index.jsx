import React, { useState } from 'react';
import PropTypes from 'prop-types';
import validator from 'validator';
import { Form, Button, Segment, Message } from 'semantic-ui-react';

const ResetPasswordEmailForm = ({ sendResetEmail: sendEmail }) => {
  const [email, setEmail] = useState('');
  const [isLoading, setLoading] = useState(false);
  const [isEmailValid, setEmailValid] = useState(false);
  const [isError, setError] = useState(false);
  const [isSuccess, setSuccess] = useState(false);

  const emailChanged = value => {
    setEmail(value);
    setEmailValid(validator.isEmail(value));
  };

  const sendResetPasswordEmail = async () => {
    if (!isEmailValid || isLoading) {
      return;
    }
    setSuccess(false);
    setError(false);
    setLoading(true);
    try {
      await sendEmail(email);
      setSuccess(true);
    } catch (error) {
      setError(true);
    }
    setLoading(false);
  };

  return (
    <Form name="sendResetEmailForm" size="large" onSubmit={sendResetPasswordEmail} error={isError} success={isSuccess}>
      <Segment>
        <Form.Input
          fluid
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          error={!isEmailValid}
          onChange={ev => emailChanged(ev.target.value)}
        />
        <Button type="submit" color="teal" fluid size="large" loading={isLoading} disabled={!isEmailValid} primary>
          Reset password
        </Button>
      </Segment>
      <Message error>
        <Message.Header>Error</Message.Header>
        Something went wrong! Please try again
      </Message>
      <Message success>
        <Message.Header>Success</Message.Header>
        <p>
          A message with link was sent to your email
        </p>
      </Message>
    </Form>
  );
};

ResetPasswordEmailForm.propTypes = {
  sendResetEmail: PropTypes.func.isRequired
};

export default ResetPasswordEmailForm;
