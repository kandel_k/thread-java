import React, { useRef, useState } from 'react';
import PropTypes from 'prop-types';
import { Icon, Input, Modal } from 'semantic-ui-react';

import styles from './styles.module.scss';

// eslint-disable-next-line no-unused-vars
const SharedPostLink = ({ postId, close, sendPost: send }) => {
  const [copied, setCopied] = useState(false);
  const [username, setUsername] = useState('');
  const [isError, setError] = useState(false);
  const [isSend, setSend] = useState(false);
  const [message, setMessage] = useState('');
  let input = useRef();

  const copyToClipboard = e => {
    input.select();
    document.execCommand('copy');
    e.target.focus();
    setMessage('Copied');
    setCopied(true);
  };

  const sendPost = async () => {
    setError(false);
    setSend(false);
    try {
      await send(postId, username);
      setMessage('Sent');
      setSend(true);
    } catch {
      setMessage('Error');
      setError(true);
    }
  };

  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Share Post</span>
        {(copied || isSend || isError) && (
          <span>
            <Icon color={!isError ? 'green' : 'red'} name={copied ? 'copy' : 'envelope'} />
            {message}
          </span>
        )}
      </Modal.Header>
      <Modal.Content>
        <Input
          fluid
          action={{
            color: 'teal',
            labelPosition: 'right',
            icon: 'copy',
            content: 'Copy',
            onClick: copyToClipboard
          }}
          value={`${window.location.origin}/share/${postId}`}
          ref={ref => { input = ref; }}
        />
      </Modal.Content>
      <Modal.Content>
        <Input
          fluid
          action={{
            color: 'teal',
            labelPosition: 'right',
            icon: 'envelope',
            content: 'Send',
            onClick: sendPost
          }}
          onChange={e => setUsername(e.target.value)}
          placeholder="Username..."
        />
      </Modal.Content>
    </Modal>
  );
};

SharedPostLink.propTypes = {
  postId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired,
  sendPost: PropTypes.func.isRequired
};

export default SharedPostLink;
