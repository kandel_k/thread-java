import React from 'react';
import { resetPassword } from 'src/containers/Profile/actions';
import Logo from 'src/components/Logo';
import { Grid, Header } from 'semantic-ui-react';
import ResetPasswordForm from '../../components/ResetPasswordForm';

const ResetPasswordPage = () => (
  <Grid textAlign="center" verticalAlign="middle" className="fill">
    <Grid.Column style={{ maxWidth: 450 }}>
      <Logo />
      <Header as="h2" color="teal" textAlign="center">
        Reset password
      </Header>
      <ResetPasswordForm resetPassword={resetPassword} />
    </Grid.Column>
  </Grid>
);

export default ResetPasswordPage;
