import React from 'react';
import { sendResetEmail } from 'src/containers/Profile/actions';
import Logo from 'src/components/Logo';
import { Grid, Header, Message } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';
import ResetPasswordEmailForm from '../../components/ResetPasswordEmailForm';

const ResetPasswordEmailPage = () => (
  <Grid textAlign="center" verticalAlign="middle" className="fill">
    <Grid.Column style={{ maxWidth: 450 }}>
      <Logo />
      <Header as="h2" color="teal" textAlign="center">
        Send recover message
      </Header>
      <ResetPasswordEmailForm sendResetEmail={sendResetEmail} />
      <Message>
        Remember the password?
        {' '}
        <NavLink exact to="/login">Sign In</NavLink>
      </Message>
    </Grid.Column>
  </Grid>
);

export default ResetPasswordEmailPage;
