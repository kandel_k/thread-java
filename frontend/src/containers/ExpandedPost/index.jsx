import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Comment as CommentUI, Header, Modal } from 'semantic-ui-react';
import moment from 'moment';
import {
  addComment,
  deleteComment,
  deletePost,
  loadPostLikes,
  reactionComment,
  reactionPost,
  toggleExpandedPost
} from 'src/containers/Thread/actions';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';

const ExpandedPost = ({
  post,
  sharePost,
  userId,
  reactionPost: reaction,
  toggleExpandedPost: toggle,
  addComment: add,
  deletePost: delPost,
  deleteComment: delComment,
  reactionComment: reactComment
}) => (
  <Modal dimmer="inverted" centered={false} open onClose={() => toggle()}>
    {post
      ? (
        <Modal.Content>
          <Post
            post={post}
            reactionPost={reaction}
            toggleExpandedPost={toggle}
            sharePost={sharePost}
            deletePost={delPost}
            loadPostLikes={loadPostLikes}
          />
          <CommentUI.Group style={{ maxWidth: '100%' }}>
            <Header as="h3" dividing>
              Comments
            </Header>
            {post.comments && post.comments
              .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
              .map(comment => (
                <Comment
                  key={comment.id}
                  comment={comment}
                  userId={userId}
                  deleteComment={delComment}
                  postId={post.id}
                  reactionComment={reactComment}
                />
              ))}
            <AddComment postId={post.id} addComment={add} />
          </CommentUI.Group>
        </Modal.Content>
      )
      : <Spinner />}
  </Modal>
);

ExpandedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  reactionPost: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired,
  reactionComment: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedPost,
  userId: rootState.profile.user.id
});

const actions = { reactionPost, toggleExpandedPost, addComment, deletePost, deleteComment, reactionComment };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpandedPost);
