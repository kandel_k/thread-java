/* eslint-disable no-unused-vars */
import React, { useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import ExpandedPost from 'src/containers/ExpandedPost';
import Post from 'src/components/Post';
import AddPost from 'src/components/AddPost';
import SharedPostLink from 'src/components/SharedPostLink';
import { Checkbox, Loader } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import {
  addPost,
  deletePost,
  loadMorePosts,
  loadPostLikes,
  loadPosts,
  reactionPost,
  sendPost,
  toggleExpandedPost
} from './actions';

import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  showNotMyPosts: false,
  showLikedPosts: false,
  showMyPosts: false,
  from: 0,
  count: 10
};

const Thread = ({
  userId,
  loadPosts: load,
  loadMorePosts: loadMore,
  posts = [],
  expandedPost,
  hasMorePosts,
  addPost: createPost,
  reactionPost: like,
  toggleExpandedPost: toggle,
  deletePost: delPost
}) => {
  const [sharedPostId, setSharedPostId] = useState(undefined);
  const [showMyPosts, setShowMyPosts] = useState(false);
  const [showNotMyPosts, setShowNotMyPosts] = useState(false);
  const [showLikedPosts, setShowLikedPosts] = useState(false);

  const applyFilter = () => {
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowMyPosts = () => {
    setShowNotMyPosts(false);
    postsFilter.showNotMyPosts = false;

    setShowMyPosts(!showMyPosts);
    postsFilter.showMyPosts = !showMyPosts;
    applyFilter();
  };

  const toggleShowNotMyPosts = () => {
    setShowMyPosts(false);
    postsFilter.showMyPosts = false;

    setShowNotMyPosts(!showNotMyPosts);
    postsFilter.showNotMyPosts = !showNotMyPosts;
    applyFilter();
  };

  const toggleShowLikedPosts = () => {
    setShowLikedPosts(!showLikedPosts);
    postsFilter.showLikedPosts = !showLikedPosts;
    applyFilter();
  };

  const getMorePosts = () => {
    loadMore(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const sharePost = id => {
    setSharedPostId(id);
  };

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost addPost={createPost} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          toggle
          label="Show only my posts"
          checked={showMyPosts}
          onChange={toggleShowMyPosts}
        />
        <Checkbox
          toggle
          label="Show not my posts"
          checked={showNotMyPosts}
          onChange={toggleShowNotMyPosts}
        />
        <Checkbox
          toggle
          label="Show liked posts"
          checked={showLikedPosts}
          onChange={toggleShowLikedPosts}
        />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Loader active inline="centered" key={0} />}
      >
        {posts.map(post => (
          <Post
            post={post}
            reactionPost={like}
            toggleExpandedPost={toggle}
            sharePost={sharePost}
            key={post.id}
            deletePost={delPost}
            loadPostLikes={loadPostLikes}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && <ExpandedPost sharePost={sharePost} />}
      {sharedPostId && (
        <SharedPostLink
          postId={sharedPostId}
          close={() => setSharedPostId(undefined)}
          sendPost={sendPost}
        />
      )}
    </div>
  );
};

Thread.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object),
  hasMorePosts: PropTypes.bool,
  expandedPost: PropTypes.objectOf(PropTypes.any),
  userId: PropTypes.string,
  loadPosts: PropTypes.func.isRequired,
  loadMorePosts: PropTypes.func.isRequired,
  reactionPost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  addPost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired
};

Thread.defaultProps = {
  posts: [],
  hasMorePosts: true,
  expandedPost: undefined,
  userId: undefined
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts,
  hasMorePosts: rootState.posts.hasMorePosts,
  expandedPost: rootState.posts.expandedPost,
  userId: rootState.profile.user.id
});

const actions = {
  loadPosts,
  loadMorePosts,
  reactionPost,
  toggleExpandedPost,
  addPost,
  deletePost
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Thread);
