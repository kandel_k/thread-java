import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import { ADD_POST, LOAD_MORE_POSTS, SET_ALL_POSTS, SET_EXPANDED_POST } from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

export const removePost = postId => (dispatch, getRootState) => {
  const { posts: { posts, expandedPost } } = getRootState();

  const updated = posts.filter(post => postId !== post.id);
  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(undefined));
  }
};

export const deletePost = postId => async dispatch => {
  const result = await postService.deletePost(postId);

  if (result?.id) {
    dispatch(removePost(postId));
  }
};

export const deleteComment = (commentId, postId) => async (dispatch, getRootState) => {
  const result = await commentService.deleteComment(commentId);

  const { posts: { posts, expandedPost } } = getRootState();

  const mapComment = post => ({
    ...post,
    comments: post.comments.filter(i => i.id !== commentId),
    commentCount: Number(post.commentCount) - 1
  });

  const mapCommentCount = post => ({
    ...post,
    commentCount: Number(post.commentCount) - 1
  });

  if (result?.id) {
    const updated = posts.map(post => (postId !== post.id ? post : mapCommentCount(post)));
    dispatch(setPostsAction(updated));
    dispatch(setExpandedPostAction(mapComment(expandedPost)));
  }
};

const countDiff = (result, isLike) => {
  let diffLikes = 0;
  let diffDislikes = 0;

  if (result?.id) {
    if (typeof result.prevIsLike === 'undefined' || result.prevIsLike === null) {
      diffLikes = isLike ? 1 : diffLikes;
      diffDislikes = !isLike ? 1 : diffDislikes;
    } else {
      diffLikes = isLike ? 1 : -1;
      diffDislikes = diffLikes * -1;
    }
  } else {
    diffLikes = isLike ? -1 : diffLikes;
    diffDislikes = !isLike ? -1 : diffDislikes;
  }

  return { diffLikes, diffDislikes };
};

export const reactionComment = (commentId, isLike) => async (dispatch, getRootState) => {
  const result = await commentService.reactionComment(commentId, isLike);
  const { diffLikes, diffDislikes } = countDiff(result, isLike);

  const mapComment = comment => ({
    ...comment,
    likeCount: Number(comment.likeCount) + diffLikes,
    dislikeCount: Number(comment.dislikeCount) + diffDislikes
  });

  const mapReactions = post => ({
    ...post,
    comments: post.comments.map(comment => (comment.id !== commentId ? comment : mapComment(comment)))
  });

  const { posts: { expandedPost } } = getRootState();

  if (expandedPost) {
    dispatch(setExpandedPostAction(mapReactions(expandedPost)));
  }
};

const mapPostReactions = (postId, diffLikes, diffDislikes) => async (dispatch, getRootState) => {
  const mapReactions = post => ({
    ...post,
    likeCount: Number(post.likeCount) + diffLikes,
    dislikeCount: Number(post.dislikeCount) + diffDislikes
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapReactions(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapReactions(expandedPost)));
  }
};

export const reactionPost = (postId, isLike) => async dispatch => {
  const result = await postService.reactionPost(postId, isLike);
  const { diffLikes, diffDislikes } = countDiff(result, isLike);

  dispatch(mapPostReactions(postId, diffLikes, diffDislikes));
};

export const loadPostLikes = async postId => {
  const response = await postService.loadPostLikes(postId);
  return response.users;
};

export const sendPost = (postId, username) => postService.sendPost(postId, username);

// TODO Create real time post reaction update
// export const setPostReaction = reaction => async dispatch => {
// };

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};
