import callWebApi from 'src/helpers/webApiHelper';

export const addComment = async request => {
  const response = await callWebApi({
    endpoint: '/api/comments',
    type: 'POST',
    request
  });
  return response.json();
};

export const getComment = async id => {
  const response = await callWebApi({
    endpoint: `/api/comments/${id}`,
    type: 'GET'
  });
  return response.json();
};

export const reactionComment = async (commentId, isLike) => {
  const response = await callWebApi({
    endpoint: '/api/comment/reaction',
    type: 'PUT',
    request: {
      commentId,
      isLike
    }
  });
  return response.json();
};

export const deleteComment = async commentId => {
  const response = await callWebApi({
    endpoint: `api/comments/delete/${commentId}`,
    type: 'DELETE'
  });
  return response.json();
};
