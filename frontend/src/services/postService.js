import callWebApi from 'src/helpers/webApiHelper';

export const getAllPosts = async filter => {
  const response = await callWebApi({
    endpoint: '/api/posts',
    type: 'GET',
    query: filter
  });
  return response.json();
};

export const addPost = async request => {
  const response = await callWebApi({
    endpoint: '/api/posts',
    type: 'POST',
    request
  });
  return response.json();
};

export const getPost = async id => {
  const response = await callWebApi({
    endpoint: `/api/posts/${id}`,
    type: 'GET'
  });
  return response.json();
};

export const reactionPost = async (postId, isLike) => {
  const response = await callWebApi({
    endpoint: '/api/postreaction',
    type: 'PUT',
    request: {
      postId,
      isLike
    }
  });
  return response.json();
};

export const sendPost = async (postId, username) => {
  await callWebApi({
    endpoint: '/api/posts/share',
    type: 'POST',
    request: {
      postId,
      username
    }
  });
};

export const loadPostLikes = async postId => {
  const response = await callWebApi({
    endpoint: '/api/postreaction/getLikes',
    type: 'GET',
    query: {
      postId
    }
  });
  return response.json();
};

export const deletePost = async postId => {
  const response = await callWebApi({
    endpoint: `api/posts/delete/${postId}`,
    type: 'DELETE'
  });
  return response.json();
};

export const updatePost = async request => {
  const response = await callWebApi({
    endpoint: 'api/update/',
    type: 'POST',
    request
  });
  return response.json();
};

// should be replaced by approppriate function
export const getPostByHash = async hash => getPost(hash);
