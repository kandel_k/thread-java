package com.threadjava.commentReactions;

import com.threadjava.comment.CommentRepository;
import com.threadjava.commentReactions.dto.ReceivedCommentReactionDto;
import com.threadjava.commentReactions.dto.ResponseCommentReactionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CommentReactionService {
    @Autowired
    private CommentReactionRepository commentReactionsRepository;
    @Autowired
    private CommentRepository commentRepository;

    public Optional<ResponseCommentReactionDto> setReaction(ReceivedCommentReactionDto commentReactionDto) {

        var reaction = commentReactionsRepository
                .getCommentReaction(commentReactionDto.getUserId(), commentReactionDto.getCommentId());

        if (reaction.isPresent()) {
            var react = reaction.get();
            if (react.getIsLike() == commentReactionDto.getIsLike()) {
                commentReactionsRepository.deleteById(react.getId());
                return Optional.empty();
            } else {
                react.setIsLike(commentReactionDto.getIsLike());
                var result = commentReactionsRepository.save(react);
                var response = CommentReactionMapper.MAPPER.reactionToCommentReactionDto(result);
                response.setPrevIsLike(reaction.get().getIsLike());
                return Optional.of(response);
            }
        } else {
            var postReaction = CommentReactionMapper.MAPPER.dtoToCommentReaction(commentReactionDto);
            var result = commentReactionsRepository.save(postReaction);
            var response = CommentReactionMapper.MAPPER.reactionToCommentReactionDto(result);
            response.setCommentOwnerId(commentRepository.findById(commentReactionDto.getCommentId()).get().getUser().getId());
            return Optional.of(response);
        }
    }
}
