package com.threadjava.commentReactions;

import com.threadjava.commentReactions.model.CommentReaction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CommentReactionRepository extends CrudRepository<CommentReaction, UUID> {
    @Query("SELECT r " +
            "FROM  CommentReaction r " +
            "WHERE r.user.id = :userId AND r.comment.id = :postId ")
    Optional<CommentReaction> getCommentReaction(@Param("userId") UUID userId, @Param("postId") UUID postId);

    List<CommentReaction> getCommentReactionsByCommentId(UUID postId);
}
