package com.threadjava.resetPassword.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class ReceivedPasswordDto {
    private UUID token;
    private String password;
}
