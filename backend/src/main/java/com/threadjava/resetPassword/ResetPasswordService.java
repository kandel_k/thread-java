package com.threadjava.resetPassword;

import com.threadjava.email.EmailService;
import com.threadjava.resetPassword.dto.ReceivedPasswordDto;
import com.threadjava.resetPassword.model.ResetPassword;
import com.threadjava.users.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class ResetPasswordService {
    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;
    @Autowired
    ResetPasswordRepository resetPasswordRepository;
    @Autowired
    UsersRepository usersRepository;
    @Autowired
    EmailService emailService;

    public boolean sendEmail(String email) {
        var user = usersRepository.findByEmail(email);
        if (user.isEmpty()) {
            return false;
        }

        String messageLink = EmailService.SITE_URL + "reset?token=";
        var passwordToken = resetPasswordRepository.findByUserEmail(email);
        if (passwordToken.isEmpty()) {
            var resetPassword = new ResetPassword(user.get());
            resetPasswordRepository.save(resetPassword);
            messageLink += resetPassword.getToken().toString();
        } else {
            messageLink += passwordToken.get().getToken().toString();
        }

        emailService.sendMessage(email, "Reset password", messageLink);
        return true;
    }

    public boolean resetPassword(ReceivedPasswordDto receivedPasswordDto) {
        var resetPassword = resetPasswordRepository.findByToken(receivedPasswordDto.getToken());

        if (resetPassword.isEmpty()) {
            return false;
        }

        var user = resetPassword.get().getUser();
        user.setPassword(bCryptPasswordEncoder.encode(receivedPasswordDto.getPassword()));
        usersRepository.save(user);
        resetPasswordRepository.delete(resetPassword.get());

        return true;
    }
}
