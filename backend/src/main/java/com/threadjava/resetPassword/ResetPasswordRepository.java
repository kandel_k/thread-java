package com.threadjava.resetPassword;

import com.threadjava.resetPassword.model.ResetPassword;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface ResetPasswordRepository extends CrudRepository<ResetPassword, UUID> {
    Optional<ResetPassword> findByUserEmail(String email);

    Optional<ResetPassword> findByToken(UUID token);
}
