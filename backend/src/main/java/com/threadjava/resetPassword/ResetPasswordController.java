package com.threadjava.resetPassword;

import com.threadjava.resetPassword.dto.ReceivedPasswordDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/auth")
public class ResetPasswordController {
    @Autowired
    private ResetPasswordService resetPasswordService;

    @PostMapping("/sendResetEmail")
    public ResponseEntity<String> sendEmail(@RequestParam(name = "email") String email) {
        return resetPasswordService.sendEmail(email)
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/reset")
    public ResponseEntity<String> resetPassword(@RequestBody ReceivedPasswordDto receivedPasswordDto) {
        return resetPasswordService.resetPassword(receivedPasswordDto)
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
