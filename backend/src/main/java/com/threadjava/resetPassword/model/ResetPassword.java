package com.threadjava.resetPassword.model;

import com.threadjava.db.BaseEntity;
import com.threadjava.users.model.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.util.UUID;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Entity
public class ResetPassword extends BaseEntity {
    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "token")
    private UUID token;

    public ResetPassword(User user) {
        this.user = user;
        this.token = UUID.randomUUID();
    }
}
