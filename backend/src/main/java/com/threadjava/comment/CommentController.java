package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsDto;
import com.threadjava.comment.dto.CommentSaveDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;
import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/comments")
public class CommentController {
    @Autowired
    private CommentService commentService;
    @Autowired
    private SimpMessagingTemplate template;

    @GetMapping("/{id}")
    public CommentDetailsDto get(@PathVariable UUID id) {
        return commentService.getCommentById(id);
    }

    @PostMapping
    public CommentDetailsDto post(@RequestBody CommentSaveDto commentDto) {
        commentDto.setUserId(getUserId());
        var response = commentService.create(commentDto);
        template.convertAndSend("/topic/comment/new/", response);
        return response;
    }

    @DeleteMapping("/delete/{id}")
    public Optional<CommentDetailsDto> delete(@PathVariable UUID id) {
        var response = commentService.delete(id);

        if (response.isPresent()) {
            template.convertAndSend("/topic/delete/"+getUserId(), "Comment has been deleted");
        }

        return response;
    }
}
