package com.threadjava.comment;

import com.threadjava.auth.TokenService;
import com.threadjava.comment.dto.CommentDetailsDto;
import com.threadjava.comment.dto.CommentSaveDto;
import com.threadjava.post.PostsRepository;
import com.threadjava.users.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class CommentService {
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private PostsRepository postsRepository;

    public CommentDetailsDto getCommentById(UUID id) {
        return commentRepository.findById(id)
                .map(CommentMapper.MAPPER::commentToCommentDetailsDto)
                .orElseThrow();
    }

    public CommentDetailsDto create(CommentSaveDto commentDto) {
        var comment = CommentMapper.MAPPER.commentSaveDtoToModel(commentDto);
        var postCreated = commentRepository.save(comment);
        var response = CommentMapper.MAPPER.commentToCommentDetailsDto(postCreated);
        response.setLikeCount(0);
        response.setDislikeCount(0);
        return response;
    }

    public Optional<CommentDetailsDto> delete(UUID id) {
        var optionalComment = commentRepository.findByIdAndUserId(id, TokenService.getUserId());

        if (optionalComment.isPresent()) {
            var comment = optionalComment.get();
            comment.setIsDelete(true);
            commentRepository.save(comment);

            return Optional.of(CommentMapper.MAPPER.commentToCommentDetailsDto(comment));
        }

        return Optional.empty();
    }
}
