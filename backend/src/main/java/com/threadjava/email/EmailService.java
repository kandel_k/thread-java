package com.threadjava.email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailService {
    @Autowired
    JavaMailSender mailSender;
    @Value("${spring.mail.username}")
    private String from;

    public static String SITE_URL = "http://localhost:3001/";

    public void sendMessage(String to, String subject, String text) {
        var message = new SimpleMailMessage();
        message.setTo("<" + to + ">");
        message.setSubject(subject);
        message.setText(text);
        message.setFrom("<" + from + ">");
        mailSender.send(message);
    }
}
