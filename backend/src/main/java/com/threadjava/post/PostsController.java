package com.threadjava.post;


import com.threadjava.post.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/posts")
public class PostsController {
    @Autowired
    private PostsService postsService;
    @Autowired
    private SimpMessagingTemplate template;

    @GetMapping
    public List<PostListDto> get(@RequestParam(defaultValue="0") Integer from,
                                 @RequestParam(defaultValue="10") Integer count,
                                 @RequestParam(required = false) UUID userId,
                                 @RequestParam(required = false) Boolean showMyPosts,
                                 @RequestParam(required = false) Boolean showNotMyPosts,
                                 @RequestParam(required = false) Boolean showLikedPosts) {
        return postsService.getAllPosts(from, count, userId, showNotMyPosts, showLikedPosts, showMyPosts);
    }

    @GetMapping("/{id}")
    public PostDetailsDto get(@PathVariable UUID id) {
        return postsService.getPostById(id);
    }

    @PostMapping
    public PostCreationResponseDto post(@RequestBody PostCreationDto postDto) {
        postDto.setUserId(getUserId());
        var item = postsService.create(postDto);
        template.convertAndSend("/topic/post/new/", item);
        return item;
    }

    @DeleteMapping("/delete/{id}")
    public Optional<PostDeletionResponseDto> delete(@PathVariable UUID id) {
        var result = postsService.deletePostById(id);

        if (result.isPresent()) {
            template.convertAndSend("/topic/post/delete/", result);
        }
        return result;
    }

    @PostMapping("/share")
    public ResponseEntity<String> sendPost(@RequestBody PostSendDto postSendDto) {
        return postsService.sendPost(postSendDto)
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
