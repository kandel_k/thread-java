package com.threadjava.post;

import com.threadjava.auth.TokenService;
import com.threadjava.comment.CommentRepository;
import com.threadjava.commentReactions.CommentReactionRepository;
import com.threadjava.commentReactions.model.CommentReaction;
import com.threadjava.email.EmailService;
import com.threadjava.post.dto.*;
import com.threadjava.post.model.Post;
import com.threadjava.users.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PostsService {
    @Autowired
    private PostsRepository postsCrudRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private CommentReactionRepository reactionRepository;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private EmailService emailService;

    public List<PostListDto> getAllPosts(Integer from, Integer count, UUID userId,
                                         Boolean isShowNotOwnPosts, Boolean isShowLikedPosts, Boolean isShowMyPosts) {
        var pageable = PageRequest.of(from / count, count);
        return postsCrudRepository
                .findAllPosts(TokenService.getUserId(), isShowLikedPosts, isShowNotOwnPosts, isShowMyPosts, pageable)
                .stream()
                .filter(post -> post.getIsDelete() == null || !post.getIsDelete())
                .map(PostMapper.MAPPER::postListToPostListDto)
                .collect(Collectors.toList());
    }

    public PostDetailsDto getPostById(UUID id) {
        var post = postsCrudRepository.findPostById(id)
                .filter(i -> i.getIsDelete() == null || !i.getIsDelete())
                .map(PostMapper.MAPPER::postToPostDetailsDto)
                .orElseThrow();

        var comments = commentRepository.findAllByPostId(id)
                .stream()
                .filter(comment -> comment.getIsDelete() == null || !comment.getIsDelete())
                .map(PostMapper.MAPPER::commentToCommentDto)
                .peek(comment -> {
                    var reactions = reactionRepository.getCommentReactionsByCommentId(comment.getId());
                    comment.setLikeCount(reactions.stream().filter(CommentReaction::getIsLike).count());
                    comment.setDislikeCount(reactions.size() - comment.getLikeCount());
                })
                .collect(Collectors.toList());
        post.setComments(comments);

        return post;
    }

    public PostCreationResponseDto create(PostCreationDto postDto) {
        Post post = PostMapper.MAPPER.postDetailsDtoToPost(postDto);
        Post postCreated = postsCrudRepository.save(post);
        return PostMapper.MAPPER.postToPostCreationResponseDto(postCreated);
    }

    public Optional<PostDeletionResponseDto> deletePostById(UUID id) {
        var optionalPost = postsCrudRepository.findPostByIdAndUserId(id, TokenService.getUserId());

        if (optionalPost.isPresent()) {
            var post = optionalPost.get();

            post.setIsDelete(true);
            postsCrudRepository.save(post);
            return Optional.of(new PostDeletionResponseDto(post.getId(), post.getUser().getId()));
        }

        return Optional.empty();
    }

    public boolean sendPost(PostSendDto postSendDto) {
        var optionalUser = usersRepository.findByUsername(postSendDto.getUsername());
        if (optionalUser.isEmpty()) {
            return false;
        }

        var currUser = usersRepository.findById(TokenService.getUserId()).get();
        new Thread(() -> {
            var link = EmailService.SITE_URL + "share/" + postSendDto.getPostId();
            var message = "Hello\n\n" + currUser.getUsername() + " shared a post with you.\n\nHave a look: " + link;
            emailService.sendMessage(optionalUser.get().getEmail(), "Share post", message);
        }).start();

        return true;
    }
}
