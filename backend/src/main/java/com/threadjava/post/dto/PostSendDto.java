package com.threadjava.post.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class PostSendDto {
    private String username;
    private UUID postId;
}
