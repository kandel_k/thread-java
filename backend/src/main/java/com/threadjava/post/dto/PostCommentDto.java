package com.threadjava.post.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class PostCommentDto {
    private UUID id;
    private String body;
    private long likeCount;
    private long dislikeCount;
    private PostUserDto user;
    private Date createdAt;
}
