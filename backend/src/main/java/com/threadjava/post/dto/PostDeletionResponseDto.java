package com.threadjava.post.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@AllArgsConstructor
@Data
public class PostDeletionResponseDto {
    private UUID id;
    private UUID userId;
}
