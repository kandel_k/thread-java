package com.threadjava.postReactions;

import com.threadjava.postReactions.dto.ReceivedPostReactionDto;
import com.threadjava.postReactions.dto.ResponsePostReactionDto;
import com.threadjava.postReactions.dto.ResponsePostUserLikes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/postreaction")
public class PostReactionController {
    @Autowired
    private PostReactionService postsService;
    @Autowired
    private SimpMessagingTemplate template;

    @PutMapping
    public Optional<ResponsePostReactionDto> setReaction(@RequestBody ReceivedPostReactionDto postReaction){
        postReaction.setUserId(getUserId());
        var reaction = postsService.setReaction(postReaction);

        template.convertAndSend("/topic/post/like/", reaction);
        return reaction;
    }

    @GetMapping("/getLikes")
    public HashMap<String, List<ResponsePostUserLikes>> getLikes(@RequestParam(name = "postId") UUID postId) {
        var response = new HashMap<String, List<ResponsePostUserLikes>>();
        response.put("users", postsService.getLikes(postId));
        return response;
    }
}
