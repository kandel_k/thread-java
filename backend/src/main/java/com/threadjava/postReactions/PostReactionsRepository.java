package com.threadjava.postReactions;

import com.threadjava.postReactions.dto.ResponsePostUserLikes;
import com.threadjava.postReactions.model.PostReaction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PostReactionsRepository extends CrudRepository<PostReaction, UUID> {
    @Query("SELECT r " +
            "FROM PostReaction r " +
            "WHERE r.user.id = :userId AND r.post.id = :postId ")
    Optional<PostReaction> getPostReaction(@Param("userId") UUID userId, @Param("postId") UUID postId);

    @Query("SELECT DISTINCT new com.threadjava.postReactions.dto.ResponsePostUserLikes(p.user.id, p.user.username) " +
            "FROM PostReaction p " +
            "WHERE p.post.id = :postId AND p.isLike = TRUE ")
    List<ResponsePostUserLikes> getLikes(@Param("postId") UUID postId);
}
