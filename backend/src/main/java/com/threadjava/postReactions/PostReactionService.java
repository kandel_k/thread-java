package com.threadjava.postReactions;

import com.threadjava.auth.TokenService;
import com.threadjava.email.EmailService;
import com.threadjava.post.PostsRepository;
import com.threadjava.postReactions.dto.ReceivedPostReactionDto;
import com.threadjava.postReactions.dto.ResponsePostReactionDto;
import com.threadjava.postReactions.dto.ResponsePostUserLikes;
import com.threadjava.users.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class PostReactionService {
    @Autowired
    private PostReactionsRepository postReactionsRepository;
    @Autowired
    private PostsRepository postsRepository;
    @Autowired
    private EmailService emailService;
    @Autowired
    private UsersRepository usersRepository;

    private void sendNotificationByEmail(ResponsePostReactionDto reaction) {
        // I use a Thread cause a sending delay can be very long and setting reaction operation can take too much time
        if (reaction.getIsLike() && TokenService.getUserId().compareTo(reaction.getPostOwnerId()) != 0) {
            System.out.println(reaction);
            System.out.println(TokenService.getUserId());
            new Thread(() -> {
                var username = usersRepository.findById(reaction.getUserId()).get().getUsername();
                var email = usersRepository.findById(reaction.getPostOwnerId()).get().getEmail();
                var text = "Hello\n\nYour post was liked by " + username
                        + ".\n\n" + EmailService.SITE_URL + "share/" + reaction.getPostId();
                emailService.sendMessage(email, "Notification", text);
            }).start();
        }
    }

    public Optional<ResponsePostReactionDto> setReaction(ReceivedPostReactionDto postReactionDto) {

        var reaction = postReactionsRepository.getPostReaction(postReactionDto.getUserId(), postReactionDto.getPostId());

        if (reaction.isPresent()) {
            var react = reaction.get();
            if (react.getIsLike() == postReactionDto.getIsLike()) {
                postReactionsRepository.deleteById(react.getId());
                return Optional.empty();
            } else {
                react.setIsLike(postReactionDto.getIsLike());
                var result = postReactionsRepository.save(react);
                var response = PostReactionMapper.MAPPER.reactionToPostReactionDto(result);
                response.setPrevIsLike(!reaction.get().getIsLike());
                response.setPostOwnerId(reaction.get().getPost().getUser().getId());

                sendNotificationByEmail(response);
                return Optional.of(response);
            }
        } else {
            var postReaction = PostReactionMapper.MAPPER.dtoToPostReaction(postReactionDto);
            var result = postReactionsRepository.save(postReaction);
            var response = PostReactionMapper.MAPPER.reactionToPostReactionDto(result);
            response.setPostOwnerId(postsRepository.findPostById(postReactionDto.getPostId()).get().getUser().getId());

            sendNotificationByEmail(response);
            return Optional.of(response);
        }
    }

    public List<ResponsePostUserLikes> getLikes(UUID postId) {
        return postReactionsRepository.getLikes(postId);
    }
}
